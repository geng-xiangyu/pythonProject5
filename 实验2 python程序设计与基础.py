'''
实验二  计算器

'''

import re
import math

class basic:
    '''
    基本四则运算（无法进行带括号的运算）
    '''
    def __init__(self, expression):
        self.expression = expression

    def addition(self, expression): #加法
        num = expression.split('+',1)
        return str(float(num[0]) + float(num[1]))

    def subtraction(self, expression): #减法
        if expression.startswith('-') :#第一个数字是负数
            expression = expression[1:]
            num = expression.split('-',1)
            num[0] = '-' + num[0]
        else :
            num = expression.split('-',1)
        return str(float(num[0]) - float(num[1]))

    def multiplication(self, expression): #乘法
        num = expression.split('*',1)
        return str(float(num[0]) * float(num[1]))

    def devision(self, expression): #除法
        num = expression.split('/',1)
        return str(float(num[0]) / float(num[1]))

    def calculation(self, expression): #判断运算式为何种运算
        expression = expression.group()
        if '+' in expression:
            return self.addition(expression)
        if '-' in expression:
            return self.subtraction(expression)
        if '*' in expression:
            return self.multiplication(expression)
        if '/' in expression:
            return self.devision(expression)

    def sorting(self, expression): #按运算顺序计算
        pattern_high =  re.compile('(\d+\.?\d*(\*|\/)+\-?\d+\.?\d*)') #高运算级
        pattern_low = re.compile('(\-?\d+\.?\d*(\+|\-)+\d+\.?\d*)') #低运算级
        if re.search(pattern_high, expression): #递归
            expression = re.sub(pattern_high, self.calculation, expression)
            return self.sorting(expression)
        elif re.search(pattern_low, expression):
            expression = re.sub(pattern_low, self.calculation, expression)
            return self.sorting(expression)
        else:
            expression
            return expression

class mod_function():
    '''
    求模运算
    '''
    def __init__(self,expression):
        self.expression = expression

    def calculation(self,expression):
        num = expression.split('%')
        return int(num[0]) % int(num[1])

class index_function():
    '''
    指数运算
    '''
    def __init__(self,expression):
        self.expression = expression

    def calculation(self,expression):
        num = expression.split('^')
        return int(num[0]) ** int(num[1])

class degree_function():
    '''
    求三角函数（输入为角度）
    '''
    def __init__(self,expression):
        self.expression = expression

    def calculation(self,expression):
        #expression = expression.group()
        if 'sin' in expression:
            num = expression.split('sin')
            degree = float(num[1])
            return math.sin(math.radians(degree))
        elif 'cos' in expression:
            num = expression.split('cos')
            degree = float(num[1])
            return math.cos(math.radians(degree))
        elif 'tan' in expression:
            num = expression.split('tan')
            degree = float(num[1])
            return math.tan(math.radians(degree))

menu='''
1--基本四则运算   2--求模运算   3--指数运算   4--求阶乘
5--角度转弧度   6--弧度转角度   7--三角函数(sin cos tan)
0.退出
'''

while(1):
    print(menu)
    a = input('请选择：')
    if a == '1':
        expression = input('请输入运算式：')
        count = basic(expression)
        result = count.sorting(expression)
        print('结果为：', result)
    elif a == '2':
        expression = input('请输入计算式如：x % y\n')
        mod = mod_function(expression)
        result = mod.calculation(expression)
        print('结果为：', result)
    elif a == '3':
        expression = input('请输入计算式如：x ^ y\n')
        index = index_function(expression)
        result = index.calculation(expression)
        print('结果为：',result)
    elif a == '4':
        z = int(input('请输入求阶乘的值：'))
        result = 1
        for i in range(1,z + 1):
            result = result * i
        print('结果为：',result)
    elif a == '5':
        degree = float(input('输入角度：'))
        print('结果为：',math.radians(degree),'rad')
    elif a == '6':
        rad = float(input('输入弧度：'))
        print('结果为：',math.degrees(rad),'度')
    elif a == '7':
        expression = input('请输入计算式：')
        degree_1 = degree_function(expression)
        result = degree_1.calculation(expression)
        print('结果为：',result)
    elif a == '0':
        break
    else:
        print('输入错误，请重新输入！')