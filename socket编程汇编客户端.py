#     实验3-客户端
#     20201113
#     耿翔宇
#     2021.5.25

import socket
import base64

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8010))
text = input("请输入要传输的内容：")
file = open('send.txt', 'a+')
file.write(text)
print('内容已写进文件')
text = text.encode('utf-8')
encode_text = base64.b32encode(text)
s.sendall(encode_text)
file.close()
data = s.recv(1024)
print("客户端的信息：", data.decode())
s.sendall("收到".encode())
print("文件内容已加密发送")
s.sendall("已接收！".encode())
s.close()
