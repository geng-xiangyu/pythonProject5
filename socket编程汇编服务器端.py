#     实验3-服务器端
#     20201113
#     耿翔宇
#     2021.5.25

import socket
import base64

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8010))
s.listen()
conn, address = s.accept()
data = conn.recv(1024)
f = open("receive.txt", "w")
data1 = base64.b32decode(data)
f.write(data1.decode())
f.close()
print("收到来自客户端加密的信息：", data.decode(), "\n已解密至receive文件中")
conn.sendall("已接收！".encode())
data1 = conn.recv(1024)
print("来自客户端的信息", data1.decode())
s.close()
