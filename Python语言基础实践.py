# （1）作者：20201113耿翔宇
# 文件名称：Python语言基础实践
# 时间：2021.3.23
# (2)
x = int(input("请输入整数x的值:\n"))  # 输入变量x的值
y = (5 * x + 8) % 26
z = 21 * (y - 8) % 26
print(y)
print(x == z)  # 判断x是否等于z
# ( 3 )
a = int(input("请输入a的值"))  # 输入首项a的值
n = int(input("请输入n的值"))  # 输入计算的项数n的值
q = int(input("请输入q的值（q≠1）"))  # 输入公比q的值
Sn = a * ((q ** n) - 1) / (q - 1)
print(Sn)
